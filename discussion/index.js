//GOAL; create a servers using exprees.js

//1.Use the require() directive to acquire the epress library and its utilities.

	const express = require('express');

//2. Create a server using only express.

	const server = express();
	//express() => will allow us to create and express application.

//3. Identify a port/adress taht will serve/host the newly connected.
	const address = 3000;

//4. Bind the application to desired desiganted port using
// using the listen(). create a method that 
//will display a response.

	server.listen(address, () => {
		console.log(`Server is running on port ${address}`)

	});
